# lox-test-suite

Test suite for the language Lox from https://craftinginterpreters.com based on the tests in https://github.com/munificent/craftinginterpreters/tree/de3efbfa40f0c26e44686fb091a1ea3b70dd92bf

## Run
```shell script
LOX=/path/to/clox-or-jlox ./test.sh test
```
