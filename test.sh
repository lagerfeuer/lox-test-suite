#!/bin/bash

set -eo pipefail

function _generate() {
  set -u
  set +e
  for src in $(find tests/ -name "*.lox"); do
    file="ref/${src}.txt"
    mkdir -p "$(basename ${file})"
    echo "${LOX} $(pwd)/${src}"
    ${LOX} "$(pwd)/${src}" &>"${file}"
  done
}

function _test() {
  set -u
  set +e
  for src in $(find tests/ -name "*.lox"); do
    file="out/${src}.txt"
    mkdir -p "$(basename ${file})"
    echo "${LOX} $(pwd)/${src}"
    ${LOX} "$(pwd)/${src}" &>"${file}"
  done

  passed=0
  total=0
  for src in $(find tests/ -name "*.lox"); do
    ref="ref/${src}.txt"
    out="out/${src}.txt"
    if ! diff "${ref}" "${out}" &>/dev/null; then
      echo "=== ${src}"
      diff "${ref}" "${out}"
    else
      ((passed++))
    fi
    ((total++))
  done
  echo "Passed: ${passed}/${total}"
}

function _main() {
  case "$1" in
    "generate" | "gen")
      _generate
      ;;
    "test")
      _test
      ;;
    *)
      echo -e "Usage:\n  LOX=/path/to/lox ${0} test" >&2
      exit 1
      ;;
  esac
}

_main "$1"
